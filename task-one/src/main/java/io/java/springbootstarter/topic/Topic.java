package io.java.springbootstarter.topic;

public class Topic {
	private String Id ;
	private String Name;
	private String Desc;
	
	
	public Topic() {
		
	}
	public Topic(String id, String name, String desc) {
		super();
		Id = id;
		Name = name;
		Desc = desc;
	}
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDesc() {
		return Desc;
	}
	public void setDesc(String desc) {
		Desc = desc;
	}
	

}
